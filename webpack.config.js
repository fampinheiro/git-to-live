const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = {
  entry: path.join(__dirname, 'app'),
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.styl$/,
      loaders: [
        'style-loader',
        'css-loader',
        'stylus-loader'
      ]
    }, {
      test: /\.png$/,
      loader: 'file-loader'
    }, {
      test: /\.(ttf|eot)$/,
      loader: 'file-loader'
    }, {
      test: /\.(jpe?g)$/i,
      loader: 'file-loader'
    }, {
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'url-loader',
      query: {
        limit: 10000,
        mimetype: 'application/font-woff'
      }
    }]
  },
  plugins: [new HtmlWebpackPlugin({
    inject: false,
    template: 'lib/template.ejs',
    title: 'Hello :)'
  })],
  output: {
    filename: 'app.js',
    path: path.join(__dirname, 'dist')
  }
}
