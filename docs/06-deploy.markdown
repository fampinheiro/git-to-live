# Gitlab

# +

# Kubernetes

---

## Gitlab + Kubernetes

### Continuous Deployment ✋

![](./media/deploy.gif)

---

## Gitlab + Kubernetes

### Continuous Deployment

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=3,labelangle=90];
    ci -> ok[color="#27ae60",taillabel="ok",labeldistance=3,labelangle=270];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
    nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];
    ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];

    ok -> server[weight=0.5,color="#27ae60"];

    server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
  }
}
```

---

## Gitlab

### Deploy

[.gitlab-ci.yml](https://gitlab.com/fampinheiro/git-to-live-source/blob/master/.gitlab-ci.yml#L47)

```
deploy:
  script:
  - docker build -t $CI_JOB_ID -f bin/dockerfile.deploy .
  - docker run
    --env KUBE_KEY_FILE="$KEY_FILE"
    ...
    $CI_JOB_ID
  stage: deploy
  tags:
  - docker
```

---

## Gitlab + Kubernetes

### Configuration

```
...
spec:
  ...
  imagePullSecrets:
  - name: gitlab-registry
```

```
$ kubectl create secret docker-registry gitlab-registry \
  --docker-username='' --docker-password='' \
  --docker-email='' --docker-server=''
```

---

## Kubernetes

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica"];
  node[fontname="Helvetica"];

  ingress_controller[color="#95a5a6",fontcolor="#95a5a6",fillcolor=white,label="ingress\ncontroller",shape=circle,style=filled]
  secret[color="#95a5a6",fontcolor="#95a5a6",fillcolor=white,label="gitlab-secret",shape=box,style=filled]

  ingress_controller -> ingress[color="#95a5a6"]
  pod -> secret[color="#95a5a6"]

  subgraph cluster1 {
    rankdir="RL";

    graph[fillcolor="#ecf0f1",fontcolor="#2c3e50",color="#ecf0f1",label="server",style=filled];

    ingress -> service[color="#27ae60"]
    service -> pod[color="#27ae60"]
    deployment -> pod[color="#27ae60"]

    deployment[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="deployment\nhello",shape=box,style=filled]
    ingress[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="ingress\nhello",shape=box,style=filled]
    pod[color="#27ae60",fontcolor=white,fillcolor="#27ae60",label="pod\nhello",margin=0.25,shape=square,style=filled]
    service[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="service\nhello",shape=circle,style=filled]
  }
}
```

---

## Gitlab + Kubernetes

### Continuous Deployment 🙌

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica"];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  ingress_controller[color="#95a5a6",fontcolor="#95a5a6",fillcolor=white,label="ingress\ncontroller",shape=circle,style=filled]
  secret[color="#95a5a6",fontcolor="#95a5a6",fillcolor=white,label="gitlab-secret",shape=box,style=filled]

  subgraph cluster0 {
    graph[fillcolor="#ecf0f1",nodesep=1,penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=3,labelangle=90];
    ci -> ok[color="#27ae60",taillabel="ok",labeldistance=3,labelangle=270];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
    nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];
    ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];

    deployment[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="deployment\nhello",shape=box,style=filled]
    ingress[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="ingress\nhello",shape=box,style=filled]
    pod[color="#27ae60",fontcolor=white,fillcolor="#27ae60",label="pod\nhello",margin=0.25,shape=square,style=filled]
    service[color="#27ae60",fillcolor="#27ae60",fontcolor=white,label="service\nhello",shape=circle,style=filled]

    ok -> ingress[color="#27ae60"]
    ok -> service[color="#27ae60"]
    ok -> deployment[color="#27ae60"]

    ingress -> service[color="#27ae60"]
    service -> pod[color="#27ae60"]
    deployment -> pod[color="#27ae60"]

    { rank=same; service; ingress; deployment; }
  }

  ingress_controller -> ingress[color="#95a5a6"]
  pod -> secret[color="#95a5a6"]
}
```

---

## Gitlab

### Pipeline

![](./media/success.gif)
