# Gitlab ✋

<img src='data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEwIiBoZWlnaHQ9IjIxMCIgdmlld0JveD0iMCAwIDIxMCAyMTAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CiAgPHBhdGggZD0iTTEwNS4wNjE0IDIwMy42NTVsMzguNjQtMTE4LjkyMWgtNzcuMjhsMzguNjQgMTE4LjkyMXoiIGZpbGw9IiNlMjQzMjkiLz4KICA8cGF0aCBkPSJNMTA1LjA2MTQgMjAzLjY1NDhsLTM4LjY0LTExOC45MjFoLTU0LjE1M2w5Mi43OTMgMTE4LjkyMXoiIGZpbGw9IiNmYzZkMjYiLz4KICA8cGF0aCBkPSJNMTIuMjY4NSA4NC43MzQxbC0xMS43NDIgMzYuMTM5Yy0xLjA3MSAzLjI5Ni4xMDIgNi45MDcgMi45MDYgOC45NDRsMTAxLjYyOSA3My44MzgtOTIuNzkzLTExOC45MjF6IiBmaWxsPSIjZmNhMzI2Ii8+CiAgPHBhdGggZD0iTTEyLjI2ODUgODQuNzM0Mmg1NC4xNTNsLTIzLjI3My03MS42MjVjLTEuMTk3LTMuNjg2LTYuNDExLTMuNjg1LTcuNjA4IDBsLTIzLjI3MiA3MS42MjV6IiBmaWxsPSIjZTI0MzI5Ii8+CiAgPHBhdGggZD0iTTEwNS4wNjE0IDIwMy42NTQ4bDM4LjY0LTExOC45MjFoNTQuMTUzbC05Mi43OTMgMTE4LjkyMXoiIGZpbGw9IiNmYzZkMjYiLz4KICA8cGF0aCBkPSJNMTk3Ljg1NDQgODQuNzM0MWwxMS43NDIgMzYuMTM5YzEuMDcxIDMuMjk2LS4xMDIgNi45MDctMi45MDYgOC45NDRsLTEwMS42MjkgNzMuODM4IDkyLjc5My0xMTguOTIxeiIgZmlsbD0iI2ZjYTMyNiIvPgogIDxwYXRoIGQ9Ik0xOTcuODU0NCA4NC43MzQyaC01NC4xNTNsMjMuMjczLTcxLjYyNWMxLjE5Ny0zLjY4NiA2LjQxMS0zLjY4NSA3LjYwOCAwbDIzLjI3MiA3MS42MjV6IiBmaWxsPSIjZTI0MzI5Ii8+Cjwvc3ZnPgo=' />

---

## Gitlab

### Continuous Integration ✋

![](./media/integrity.gif)

---

## Gitlab

### Continuous Integration

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci[fillcolor=white,label="magic",margin=0.1,shape=diamond,style=dashed,style=filled];
  }

  ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=2,labelangle=90];
  ci -> ok[color="#27ae60",taillabel="ok",labeldistance=2,labelangle=270];

  ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];
  nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];

  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```

---

## Gitlab

### Runners

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
  }

  ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=2,labelangle=90];
  ci -> ok[color="#27ae60",taillabel="ok",labeldistance=2,labelangle=270];

  ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];
  nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];

  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```

---

## Gitlab

### Runner + docker

```
$ export GL_RUNNER_NAME=gitlab-runner
$ docker run -d --name $GL_RUNNER_NAME --restart=on-failure \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /var/lib/gitlab-runner/$GL_RUNNER_NAME/config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

```
$ export CI_GITLAB_URL= # https://gitlab.com/ci
$ export CI_RUNNER_TOKEN= # "settings > ci/cd pipelines"
$ docker exec -it $GL_RUNNER_NAME gitlab-runner register -n \
  --url $CI_GITLAB_URL --tag-list "docker" \
  --registration-token $CI_RUNNER_TOKEN \
  --executor docker --description "docker runner" \
  --docker-image "docker:latest" --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
```

---

## Gitlab

### Test

[.gitlab-ci.yml](https://gitlab.com/fampinheiro/git-to-live-source/blob/master/.gitlab-ci.yml#L84)

```
test:dev:
  script:
  - docker build -t $GL_IMAGE_NAME:$GL_IMAGE_TAG .
  - docker run $GL_IMAGE_NAME:$GL_IMAGE_TAG npm run test
  stage: test
```

---

## Gitlab

### Continuous Integration 🙌

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
  }

  ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=2,labelangle=90];
  ci -> ok[color="#27ae60",taillabel="ok",labeldistance=2,labelangle=270];

  ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];
  nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];

  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```

---

## Gitlab

### Continuous Delivery ✋

![](./media/delivery.gif)

---

## Gitlab

### Continuous Delivery

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=3,labelangle=90];
    ci -> ok[color="#27ae60",taillabel="ok",labeldistance=3,labelangle=270];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
    nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];
    ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];
  }


  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```

---

## Gitlab

### Build

[.gitlab-ci.yml](https://gitlab.com/fampinheiro/git-to-live-source/blob/master/.gitlab-ci.yml#L43)

```
build:
  script:
  - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY
  - docker build -t $GL_IMAGE_NAME:$GL_IMAGE_TAG .
  - docker push $GL_IMAGE_NAME:$GL_IMAGE_TAG
  stage: build
```

---

## Gitlab

### Continuous Delivery 🙌

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];

  subgraph cluster_0 {
    graph[fillcolor="#ecf0f1",penwidth=0,style=filled]
    code[fillcolor=white,label="gitlab.com",shape=square,style=filled];
    code -> ci[weight=0.5];

    ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=3,labelangle=90];
    ci -> ok[color="#27ae60",taillabel="ok",labeldistance=3,labelangle=270];

    ci[fillcolor=white,label="runner",margin=0.1,shape=diamond,style=dashed,style=filled];
    nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];
    ok[color="#27ae60",fillcolor=white,fontcolor="#27ae60",label="deployable\nartifact",shape=square,style=filled];
  }


  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```
