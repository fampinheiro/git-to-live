# Kubernetes ✋

<img src="./media/kube.png" alt="kubernetes logo" style="width: 25%;">

---

## Kubernetes

### Objects

[Deployment](https://kubernetes.io/docs/user-guide/deployments/)

[Services](https://kubernetes.io/docs/user-guide/services/)

[Secrets](https://kubernetes.io/docs/user-guide/secrets/)

[Job](https://kubernetes.io/docs/user-guide/jobs/)

[...](https://kubernetes.io/docs/user-guide/)

---

## Kubernetes

### Configuration

```
apiVersion: v1
kind: Pod
metadata:
  ...
spec:
  containers:
  - name: hello
    image: registry.gitlab.com/fampinheiro/hello:1dcc0c
    ...
```

---

## Kubernetes

### kubectl

```
$ kubectl create <options> [--save-config]
```

```
$ kubectl apply <options>
```

```
$ kubectl delete <options>
```
