# Development Process

---

## Development Process

```dot
digraph Cluster {
  rankdir="LR";

  edge[fontname="Helvetica"];
  graph[fontname="Helvetica",splines=curved];
  node[fontname="Helvetica"];

  git -> code[label="push",weight=0.1];
  git[label="your\ncode",shape=circle,fillcolor="#2980b9",fontcolor=white,penwidth=0,style=filled];
  code[shape=square,label="version\ncontrol"];

  code -> ci[weight=0.9];

  ci -> nok[color="#e74c3c",taillabel="fail",labeldistance=3,labelangle=45];
  ci -> ok[color="#27ae60",taillabel="ok",labeldistance=3,labelangle=315];

  ci[label="magic",margin=0.1,shape=diamond];
  ok[color="#27ae60",fontcolor="#27ae60",label="deployable\nartifact",shape=square];
  nok[label="nok",shape=circle,fillcolor="#e74c3c",fontcolor=white,penwidth=0,style=filled];

  ok -> server[weight=0.5,color="#27ae60"];

  server[fillcolor="#27ae60",fontcolor=white,shape=circle,penwidth=0,style=filled];
}
```
