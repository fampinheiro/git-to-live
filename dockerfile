FROM node:7-alpine

EXPOSE 3000

WORKDIR /usr/local/app

COPY package.json .
RUN npm install --production

# copy the remaining files
COPY . .

CMD npm run build
