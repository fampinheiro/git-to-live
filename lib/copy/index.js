const copy = require('copy-dir')
const path = require('path')

const media = path.join(__dirname, '../../app/media')
const dist = path.join(__dirname, '../../dist/media')
module.exports = function (cb) {
  return copy(media, dist, cb)
}
