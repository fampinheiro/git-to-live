const http = require('http')
const Server = require('node-static').Server
const server = new Server('./dist', {
  cache: false
})

http
  .createServer(function (request, response) {
    request
      .addListener('end', function () {
        server.serve(request, response)
      })
      .resume()
  })
  .listen(8080)
