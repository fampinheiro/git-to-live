const build = require('./build')
const copy = require('./copy')
const debug = require('debug')('build')
const ejs = require('ejs')
const fs = require('fs')
const path = require('path')
const series = require('run-series')
const waterfall = require('run-waterfall')
const webpack = require('webpack')

series([persist, copy, serve], (err, results) => {
  if (err) {
    throw err
  }

  const stats = results[2]

  debug(stats.compilation)
  if (stats.compilation.errors.length) {
    console.log(stats.compilation.errors)
    process.exit(1)
  }
  console.log('done')
})

function persist (cb) {
  return build((err, contents) => {
    if (err) {
      return cb(err)
    }

    return waterfall([readFile, compile, writeFile], cb)

    function compile (tpl, cb) {
      const compiler = ejs.compile(tpl)
      const content = contents.map(e => wrap(e)).join('\n')
      return cb(
        null,
        compiler({
          content
        })
      )

      function wrap (content) {
        if (!content) {
          return ''
        }

        if (!Array.isArray(content)) {
          return `<section>${content}</section>`
        }

        return `<section>${content.map(e => wrap(e)).join('\n')}</section>`
      }
    }
  })

  function readFile (cb) {
    const filename = path.join(__dirname, 'layout.ejs')
    return fs.readFile(filename, 'utf8', cb)
  }

  function writeFile (content, cb) {
    const filename = path.join(__dirname, 'template.ejs')
    return fs.writeFile(filename, content, 'utf8', err => {
      return cb(err)
    })
  }
}

function serve (cb) {
  const wp = webpack(require('../webpack.config.js'))
  return wp.run(cb)
}
