const hljs = require('highlight.js')
const marked = require('marked')

marked.setOptions({
  highlight: function (code) {
    return hljs.highlightAuto(code).value
  }
})

module.exports = function transform (content, cb) {
  const parts = content.split('\n\n---\n\n')
  return cb(null, parts.map(part => {
    const [text, notes = ''] = part.split('Notes:')
    return marked([
      text,
      `<aside class='notes'>\n${marked(notes)}</aside>\n`
    ].join('\n\n'))
  }))
}
