const fs = require('fs')
const mkdirp = require('mkdirp')
const path = require('path')
const dot = require('./dot')
const series = require('run-series')
const render = require('./render')
const waterfall = require('run-waterfall')

const root = path.join(__dirname, '..', '..')
module.exports = function build (cb) {
  const src = path.join(root, 'docs')
  return fs.readdir(src, 'utf8', readdir)
  function readdir (err, files) {
    if (err) {
      return cb(err)
    }

    const tasks = files
      .filter(file => path.extname(file) === '.markdown')
      .map(file => analyze.bind(null, path.join(src, file)))

    const dist = mkdirp.bind(null, path.join(root, 'dist'))
    return series([dist, ...tasks], (err, results) => {
      if (err) {
        return cb(err)
      }

      return cb(null, results.slice(1))
    })
  }
}

function analyze (file, cb) {
  return fs.access(file, fs.constants.R_OK, err => {
    if (err) {
      return cb(err)
    }

    return waterfall([readFile, dot, render], cb)
  })

  function readFile (cb) {
    return fs.readFile(file, 'utf8', cb)
  }
}
