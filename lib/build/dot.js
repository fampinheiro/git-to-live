const arr = require('async-regex-replace')
const fs = require('fs')
const hasha = require('hasha')
const path = require('path')
const viz = require('viz.js')

const engines = ['circo', 'dot', 'fdp', 'neato', 'osage', 'twopi']
const re = new RegExp(/```([a-z]*)\n([\s\S]*?)\n```/g)
const root = path.join(__dirname, '..', '..')
module.exports = function replacer (content, cb) {
  return arr.replace(re, content, replace, result)
  function result (err, result) {
    if (err) {
      return cb(err)
    }
    return cb(null, result)
  }
}

function replace (...args) {
  const [input, type, graph, cb] = args
  if (engines.indexOf(type) === -1) {
    return cb(null, input)
  }

  return convert(type, graph, cb)
}

function convert (type, graph, cb) {
  const options = {
    format: 'svg',
    engine: type
  }

  const content = viz(graph, options)
  const dst = `${hasha(type + content)}.svg`
  return fs.writeFile(path.join(root, 'dist', dst), content, 'utf8', err => {
    if (err) {
      return cb(err)
    }
    return cb(null, `![](./${dst})`)
  })
}
