require('./styles/main.styl')

global.Reveal = require('reveal.js')
global.Reveal.initialize({
  controls: false,
  height: '100%',
  history: true,
  margin: 0.1,
  transition: 'none'
})
